import numpy as np

class Perceptron:
    def __init__(self):
        trainX = np.array([[1, 0, 0],
                        [0, 1, 0],
                        [1, 0, 1],
                        [1, 1, 1],
                        [0, 0, 1]])

        trainY = np.array([[1, 0, 1, 1, 0]]).T

        np.random.seed(1)
        self.w = 2 * np.random.random((3, 1)) - 1

        print("Initial weights:")
        print(self.w)

        Linput = trainX
        Loutput = Perceptron.sigmoid(np.dot(Linput, self.w))

        print("Initial results:")
        print(Loutput)

        # Обратное распространение
        for i in range(20000):
            Linput = trainX
            Loutput = Perceptron.sigmoid(np.dot(Linput, self.w))

            err = trainY - Loutput
            correction = np.dot(Linput.T, err * (Loutput * (1 - Loutput)))

            self.w += correction

        print("Educated weights:")
        print(self.w)

        print("Educated results:")
        print(Loutput)
    
    def predict(self, data):
        Linput = data
        Loutput = Perceptron.sigmoid(np.dot(Linput, self.w))
        return Loutput
    
    @staticmethod
    def sigmoid(x):
            return 1 / (1 + np.exp(-x))