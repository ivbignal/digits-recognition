import numpy as np

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

import sys
import numpy
from PIL import Image
from PIL.ImageQt import ImageQt
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QMessageBox, QAction, qApp, QInputDialog
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import Qt

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout, Activation, BatchNormalization, AveragePooling2D
from tensorflow.keras.optimizers import SGD, RMSprop, Adam
import tensorflow as tf

import logging

logging.getLogger().setLevel(logging.INFO)
tf.logging.set_verbosity(tf.logging.INFO)
tf.get_logger().setLevel(logging.INFO)

def log_uncaught_exceptions(ex_cls, ex, tb):
    text = '{}: {}:\n\n'.format(ex_cls.__name__, ex)
    import traceback
    text += ''.join(traceback.format_tb(tb))

    QMessageBox.critical(None, 'Ошибка!', text)
    quit()

sys.excepthook = log_uncaught_exceptions


class ShowImage(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setMouseTracking(True)

        self.model = Sequential()
        self.model.add(Dense(1600, input_dim=1600, activation='relu'))
        self.model.add(Dense(10, activation='sigmoid'))
        self.model.compile(loss='binary_crossentropy', optimizer=SGD(lr=0.1))

        self.interface()

    def interface(self):
        self.setWindowTitle("Neural number")
        self.setGeometry(700, 400, 400, 550)

        self.initMenu()

        self.canvas = QLabel(self)
        self.canvas.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
        self.canvas.setGeometry(0, 20, 400, 400)
        
        self.clearArea()

    def draw(self):
        self.canvas.setPixmap(QPixmap(QImage(ImageQt(Image.fromarray(self.pixel)))).scaled(400, 400))
    
    def clearArea(self):
        self.pixel = numpy.zeros((40, 40)).astype('uint8')
        self.draw()
    
    def teachNetwork(self):
        num, ok = QInputDialog.getText(self, "Teaching", "Correct value:")
        if ok:
            self.data = " ".join(str(0 if n == 0 else 1) for n in self.pixel.flatten()) + " " + num
            with open("train.txt", "a") as f:
                f.write(self.data + "\n")
            self.clearArea()
    
    def trainNetwork(self):
        trainset = np.genfromtxt("train.txt")
        trainkey = np.zeros((trainset.shape[0],10))
        for i, n in enumerate(trainset[:, -1]):
            trainkey[i][int(n)] = 1
        trainset = trainset[:, :-1]
        # numpy.set_printoptions(threshold=sys.maxsize)
        # print(trainset, trainkey)
        
        self.model.fit(trainset, trainkey, batch_size=1, epochs=100, verbose=0)
    
    def predict(self):
        self.prediction = self.model.predict_proba(np.array([self.pixel.flatten()]))
        print(self.prediction)

    def initMenu(self):
        statusbar = self.statusBar()

        exitAction = QAction("&Exit", self)
        exitAction.setShortcut("Ctrl+Q")
        exitAction.setStatusTip("Exit application")
        exitAction.triggered.connect(qApp.quit)
        clearAction = QAction("&Clear all", self)
        clearAction.setShortcut("Ctrl+C")
        clearAction.setStatusTip("Clear draw area")
        clearAction.triggered.connect(self.clearArea)
        teachAction = QAction("&Teach", self)
        teachAction.setShortcut("Ctrl+T")
        teachAction.setStatusTip("Add data for training set")
        teachAction.triggered.connect(self.teachNetwork)
        trainAction = QAction("&Train", self)
        trainAction.setShortcut("Ctrl+Shift+T")
        trainAction.setStatusTip("Train neural network using training set")
        trainAction.triggered.connect(self.trainNetwork)
        predictAction = QAction("&Predict", self)
        predictAction.setShortcut("Ctrl+P")
        predictAction.setStatusTip("Predict result using trained network")
        predictAction.triggered.connect(self.predict)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu("&File")
        fileMenu.addAction(exitAction)
        editMenu = menubar.addMenu("&Edit")
        editMenu.addAction(teachAction)
        editMenu.addAction(trainAction)
        editMenu.addAction(predictAction)
        editMenu.addAction(clearAction)
    
    def mouseMoveEvent(self, event):
        x = event.x()
        y = event.y()
        if (y < 420 and y > 20 and x > 0 and x < 400):
            x = event.x() // 10
            y = (event.y() - 20) // 10
            if event.buttons() == Qt.LeftButton:
                self.pixel[y][x] = 255
            if event.buttons() == Qt.RightButton:
                self.pixel[y][x] = 0
            self.draw()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    my_application = ShowImage()
    my_application.show()
    sys.exit(app.exec_())